package com.Y2.Clinical.G12.repositories;


import com.Y2.Clinical.G12.data.users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by c1632067 on 11/30/2017.
 */

public interface userRepository extends JpaRepository<users, Long> {
    List<users> findAll();

//    call the stored procedures and supply parameters if required
    @Autowired
    @Query(value="{call getUser(:myUsername,:myPassword)}", nativeQuery = true)
    Integer getUser(@Param("myUsername")String myUsername,@Param("myPassword") String myPassword);

    @Modifying
    @Query(value = "{call insertUsers(:username,:password)}", nativeQuery = true)
    @Transactional
    void insertUser(@Param("username") String username,@Param("password") String password);

    @Autowired
    @Query(value="Select * from questionnaire_user where user_id=:id", nativeQuery = true)
    List<users> getUserLoggedIn(@Param("id")Integer id);


}


