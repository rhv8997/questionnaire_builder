package com.Y2.Clinical.G12.repositories;

import com.Y2.Clinical.G12.data.user_answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by c1644043 on 30/11/2017.
 */


@Repository("usersAnswersRepo")
public interface usersAnswersRepo extends JpaRepository<user_answer, Long> {
    List<user_answer> findAll();
//    call the stored procedures and supply parameters if required

    @Modifying
    @Query(value = "{call insertUsersAnswers(:answer,:questionId,:questionnaireId)}", nativeQuery = true)
    @Transactional
    void insertUsersAnswers(@Param("answer") String answer, @Param("questionId") Integer questionId, @Param("questionnaireId") Integer questionnaireId);






    @Autowired
    @Query(value="{call getUsersAnswers(:questionnaireId)} ", nativeQuery = true)
    List<user_answer> getUsersAnswers(@Param("questionnaireId")String questionnaireId);

}

