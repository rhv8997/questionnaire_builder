package com.Y2.Clinical.G12.forms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by c1632067 on 11/30/2017.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class loginForm {
    private String username;
    private String password;
}
