package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.data.user_answer;
import com.Y2.Clinical.G12.repositories.answerRepo;
import com.Y2.Clinical.G12.repositories.usersAnswersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service("UsersAnswerService")
@Transactional
public class usersAnswersServiceImpl implements usersAnswersService {

    @Autowired
    private usersAnswersRepo usersAnswersRepo;

    @Autowired
    public usersAnswersServiceImpl(answerRepo answerRepo){
        this.usersAnswersRepo = usersAnswersRepo;
    }

    @Override
    public void insertUsersAnswers(String answer, Integer questionId, Integer questionnaireId) {
        usersAnswersRepo.insertUsersAnswers(answer, questionId, questionnaireId);

    }



    @Override
    public List<user_answer> getUsersAnswers(String questionnaireId) {
        return usersAnswersRepo.getUsersAnswers(questionnaireId);

    }
}


