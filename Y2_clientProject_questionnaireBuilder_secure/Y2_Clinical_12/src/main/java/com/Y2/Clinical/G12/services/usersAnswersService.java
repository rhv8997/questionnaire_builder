package com.Y2.Clinical.G12.services;


import com.Y2.Clinical.G12.data.user_answer;

import java.util.List;

public interface usersAnswersService {

    void insertUsersAnswers(String answer, Integer questionId, Integer questionnaireId);

    List<user_answer> getUsersAnswers(String questionnaireId);


}

