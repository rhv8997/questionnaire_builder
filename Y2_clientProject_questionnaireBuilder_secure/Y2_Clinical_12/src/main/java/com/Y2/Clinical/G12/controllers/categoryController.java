package com.Y2.Clinical.G12.controllers;

import com.Y2.Clinical.G12.data.categories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.Y2.Clinical.G12.services.categoryNameService;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by c1632067 on 12/12/2017.
 */
@RestController
@RequestMapping (path = "/api")
public class categoryController {


    private categoryNameService categoryNameService;

    @Autowired
    public categoryController(categoryNameService categoryNameService) {
        this.setCategoryNameService(categoryNameService);

    }

    @RequestMapping(path = "/category/getCategoryName/{categoryName}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Object> getQuestionnairesByCategoryName(@PathVariable("categoryName")String categoryName ){
     return new ResponseEntity<>(getCategoryNameService().getQuestionnaireByCategoryName(categoryName), HttpStatus.OK);


    }

    public com.Y2.Clinical.G12.services.categoryNameService getCategoryNameService() {
        return categoryNameService;
    }

    public void setCategoryNameService(com.Y2.Clinical.G12.services.categoryNameService categoryNameService) {
        this.categoryNameService = categoryNameService;
    }


}
