package com.Y2.Clinical.G12.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "multiple_choice_answer")
public class answers {

    @Id
    @Column(name = "answer_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer answerId;

    @Column(name = "question_question_id")
    private Integer questionId;

    @Column(name = "answer_content")
    private String answer;
}
