package com.Y2.Clinical.G12.repositories;

import com.Y2.Clinical.G12.data.questionnaires;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
@Repository("questionnaireRepo")
public interface questionnaireRepo extends JpaRepository<questionnaires, Long> {
    List<questionnaires> findAll();
    List<questionnaires> findByCategoryName(String categoryName);
//    call the stored procedures and supply parameters if required

    @Modifying
    @Query(value = "{call deleteQuestionnaire(:questionnaireTitle)}", nativeQuery = true)
    @Transactional
    void deleteQuestionnaire(@Param("questionnaireTitle") String questionnaireTitle);

    @Modifying
    @Query(value = "{call insertQuestionnaire(:questionnaire)}", nativeQuery = true)
    @Transactional
    void insertQuestionnaire(@Param("questionnaire") String questionnaire);


    @Modifying
    @Query(value = "{call testQuestionnaireDeletion(:qt)}", nativeQuery = true)
    @Transactional
    void testQuestionnaireDeletion(@Param("qt") String qt);

    @Autowired
    @Query(value="{call getQuestionnaireTitle(:ref)} ", nativeQuery = true)
    String getQuestionnaireTitle(@Param("ref")Integer ref);

    @Autowired
    @Query(value="{call insertLinkId(:link_id, :qid)} ", nativeQuery = true)
    @Transactional
    void insertLinkId(@Param("link_id")String link_id,@Param("qid")Integer qid);
}
