package com.Y2.Clinical.G12.repositories;

import com.Y2.Clinical.G12.data.categories;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by c1632067 on 12/7/2017.
 */
public interface categoryRepo extends JpaRepository<categories, Long> {
    List<categories> findAll();
//    call the stored procedures and supply parameters if required

    List<categories> findAllByCategoryName(String categoryName);
}
