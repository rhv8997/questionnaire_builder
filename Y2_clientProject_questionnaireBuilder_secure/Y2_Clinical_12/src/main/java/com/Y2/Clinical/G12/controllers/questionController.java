package com.Y2.Clinical.G12.controllers;

import com.Y2.Clinical.G12.data.questions;
import com.Y2.Clinical.G12.services.questionService;
import com.Y2.Clinical.G12.services.answerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by c1632067 on 12/12/2017.
 */
@RestController
@RequestMapping (path = "/")
public class questionController {

    @Autowired
    private questionService questionService;

    @Autowired
    private answerService answerService;


    @Autowired
    public questionController(questionService questionService) {
        this.setQuestionService(questionService);
    }

    @RequestMapping(value = "/Question/{questionTitle}/url/{url}")
    public RedirectView deleteQuestion(@PathVariable("questionTitle") String questionTitle, @PathVariable("url") String url) {
//        if a question isnt the content of the dropdown, insert it into the delete method and reload the builder
        try{
        if (!(questionTitle.contentEquals("PLEASE CHOOSE A QUESTION"))) {
            getQuestionService().deleteQuestion(questionTitle);
        }
        RedirectView rv = new RedirectView();
        rv.setUrl("/questionnaireBuild"+ url);
        return rv;
//        go to error page

        }catch(Exception e){
            RedirectView rv = new RedirectView();
            rv.setUrl("/generic-error");
            return rv;
        }
    }

//insert the different types of questions by extracting the questionnaire ID from the URL and using the insert question service
    @RequestMapping(value = "/longQuestion", method = RequestMethod.POST)
    public RedirectView insertLongQuestion(@RequestParam("question") String question, @RequestParam("selectQuestion") String selectQuestion,@RequestParam("mlOldQuestion") String oldQuestion, @RequestParam("currentURL") String url) {
        try{
        String[] urlParams = url.split("&\\?");
        String[] idParam = urlParams[1].split("=");
        String q_hash = idParam[1];
        Integer qId = getQuestionService().getQuestionnaireId(q_hash);
        if (oldQuestion.length() == 0) {
            if (!(question.toString().equals(""))) {
                getQuestionService().insertQuestion(question, 2, qId);
            } else {
                if (!(selectQuestion.toString().equals("PLEASE CHOOSE A QUESTION"))) {
                    getQuestionService().insertQuestion(selectQuestion, 2, qId);
                }
            }
        } else {
            getQuestionService().updateQuestionTitle(question, oldQuestion);
        }
        RedirectView rv = new RedirectView();
        rv.setUrl(url);
        return rv;
        }catch(Exception e){
            RedirectView rv = new RedirectView();
            rv.setUrl("/generic-error");
            return rv;
        }
    }

    @RequestMapping(value = "/shortQuestion", method = RequestMethod.POST)
    public RedirectView insertShortQuestion(@RequestParam("question") String question, @RequestParam("selectQuestion") String selectQuestion,@RequestParam("singleOldQuestion") String oldQuestion, @RequestParam("currentURL") String url) {
        try{
        String[] urlParams = url.split("&\\?");
        String[] idParam = urlParams[1].split("=");
        String q_hash = idParam[1];
        Integer qId = getQuestionService().getQuestionnaireId(q_hash);

        if (oldQuestion.length() == 0) {
            if (!(question.toString().equals(""))) {
                getQuestionService().insertQuestion(question, 1, qId);
            } else {
                if (!(selectQuestion.toString().equals("PLEASE CHOOSE A QUESTION"))) {
                    getQuestionService().insertQuestion(selectQuestion, 1,qId);
                }
            }
        } else {
            getQuestionService().updateQuestionTitle(question, oldQuestion);
        }
        RedirectView rv = new RedirectView();
        rv.setUrl(url);
        return rv;
        }catch(Exception e){
            RedirectView rv = new RedirectView();
            rv.setUrl("/generic-error");
            return rv;
        }


    }


    @RequestMapping(value = "/multiChoiceQuestion", method = RequestMethod.POST)
    public RedirectView insertMultiChoiceQuestion(@RequestParam("allQuestions") String question, @RequestParam("multiOldQuestion") String oldQuestion, @RequestParam("currentURL") String url){
        try{
        String[] urlParams = url.split("&\\?");
        String[] idParam = urlParams[1].split("=");
        String q_hash = idParam[1];
        Integer qId = getQuestionService().getQuestionnaireId(q_hash);
        if (oldQuestion.length() == 0) {
            getQuestionService().insertQuestion(question, 3, qId);



        } else {
            getQuestionService().updateQuestionTitle(question, oldQuestion);
        }
        RedirectView rv = new RedirectView();
        rv.setUrl(url);
        return rv;
        }catch(Exception e){
            RedirectView rv = new RedirectView();
            rv.setUrl("/generic-error");
            return rv;
        }
    }


//    Get a list of questions in a stream if questions exist for the given questionnaire
    @RequestMapping(path = "/questionsForQuestionnaire", method = RequestMethod.GET)
    public List<questions> returnQuestionsWithTypes(@RequestParam("questionnaireId") String questionnaireId) {
        List<questions> questions;
        questions = getQuestionService().getQuestionsForQuestionnaire(questionnaireId);
        return questions.stream().collect(Collectors.toList());

    }

































    public void setQuestionService(com.Y2.Clinical.G12.services.questionService questionService) {
        this.questionService = questionService;
    }

    public com.Y2.Clinical.G12.services.questionService getQuestionService() {
        return questionService;
    }

    public void setAnswerService(com.Y2.Clinical.G12.services.answerService answerService) {
        this.answerService = answerService;
    }

    public com.Y2.Clinical.G12.services.answerService getAnswerService() {
        return answerService;
    }


}
