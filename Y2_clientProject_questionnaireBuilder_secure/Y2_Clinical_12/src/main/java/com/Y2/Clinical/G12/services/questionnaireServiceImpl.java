package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.data.questionnaires;
import com.Y2.Clinical.G12.repositories.questionnaireRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service("questionnaireService")
@Transactional
public class questionnaireServiceImpl implements questionnaireService {

    @Autowired
    private questionnaireRepo questionnaireRepo;

    @Autowired
    public questionnaireServiceImpl(questionnaireRepo questionnaireRepo){
        this.questionnaireRepo = questionnaireRepo;
    }
    
    @Override
    public List<questionnaires> returnQuestionnaires() {
            return questionnaireRepo.findAll();
        }

    @Override
    public void insertQuestionnaires(String questionnaire) {
        questionnaireRepo.insertQuestionnaire(questionnaire);

    }
    @Override
    public void deleteQuestionnaire(String questionnaireTitle) {
        questionnaireRepo.deleteQuestionnaire(questionnaireTitle);

    }

    @Override
    public String getQuestionnaireTitle(Integer ref) {
        return questionnaireRepo.getQuestionnaireTitle(ref);
    }



}



