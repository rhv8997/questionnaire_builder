package com.Y2.Clinical.G12;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class mainApplication {

    public static void main (String[] args) {
        SpringApplication.run(mainApplication.class, args);
    }
}
