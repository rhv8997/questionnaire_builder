package com.Y2.Clinical.G12.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by c1632067 on 12/7/2017.
 */@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "category")
public class categories {
    @Id
    @Column(name = "category_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer categoryId;

    @Column(name = "category_name")
    private String categoryName;

}