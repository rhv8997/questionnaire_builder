package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.data.categories;
import com.Y2.Clinical.G12.data.questionnaires;
import com.Y2.Clinical.G12.repositories.categoryRepo;
import com.Y2.Clinical.G12.repositories.questionnaireRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by c1632067 on 12/12/2017.
 */
    @Service
    public class categoryNameServiceImpl implements categoryNameService {
        @Autowired
        private questionnaireRepo questionnaireRepo;

        @Autowired
        private categoryRepo categoryRepo;

        @Autowired
        public categoryNameServiceImpl(questionnaireRepo questionnaireRepo){
            this.questionnaireRepo = questionnaireRepo;
        }

        @Override
        public List<questionnaires> getQuestionnaireByCategoryName (String categoryName){
            return questionnaireRepo.findByCategoryName(categoryName);
        }

    @Override
    public List<categories> getAllCategories() {
        return categoryRepo.findAll();
    }


}
