package com.Y2.Clinical.G12.config;
import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class RequestRouter extends WebMvcConfigurerAdapter {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

        registry.addViewController("/questionnaireBuild/**").setViewName("forward:/questionnaireBuild.html");
        registry.addViewController("/index/**").setViewName("forward:/index.html");
        registry.addViewController("/login/**").setViewName("forward:/loginIndex.html");
        registry.addViewController("/signUp-user/**").setViewName("forward:/signUp-user.html");
        registry.addViewController("/passwords-dont-match/**").setViewName("forward:/passwords-dont-match.html");


        registry.addViewController("/signUp-complete/**").setViewName("forward:/signUp-complete.html");
        registry.addViewController("/answerQuestionnaire/**").setViewName("forward:/questionnaireRender.html");
        registry.addViewController("/questionnaireComplete/**").setViewName("forward:/questionnaireComplete.html");
        registry.addViewController("/login-error/**").setViewName("forward:/login-error.html");
        registry.addViewController("/generic-error/**").setViewName("forward:/generic-error.html");


    }

}