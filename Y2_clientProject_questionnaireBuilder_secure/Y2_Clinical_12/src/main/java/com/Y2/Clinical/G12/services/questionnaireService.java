package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.data.questionnaires;

import java.util.List;

public interface questionnaireService {

    List<questionnaires> returnQuestionnaires();
    void insertQuestionnaires(String questionnaire);
    void deleteQuestionnaire(String questionnaireTitle);
    String getQuestionnaireTitle(Integer ref);


}


