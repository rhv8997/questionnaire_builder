package com.Y2.Clinical.G12.controllers;

import com.Y2.Clinical.G12.data.users;
import com.Y2.Clinical.G12.forms.loginForm;
import com.Y2.Clinical.G12.services.questionService;
import com.Y2.Clinical.G12.services.userService;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;


/**
 * Created by c1632067 on 11/23/2017.
 */

@RestController
@RequestMapping(path = "/")
public class loginController {



    @Autowired
    private com.Y2.Clinical.G12.services.userService userService;


    @ResponseBody
    @RequestMapping(value = "/myLogin", method  = {RequestMethod.GET, RequestMethod.POST})
    public RedirectView getUser(@RequestParam("username") String myUsername, @RequestParam("password") String myPassword, HttpSession session) throws SQLException {
        /*service used to retrieve the ID of rhe user with the username and password*/
        Integer userId = getUserService().getUser(myUsername, myPassword);
        try{
//            If the user exists, a user object is made and user attributes are set.
            if (userId>0){
                users user  = new users(myUsername,myPassword);
                session.setAttribute("LoggedInUser", user);
                session.setAttribute("userId", userId);
                session.setAttribute("username", myUsername);
                session.setAttribute("password", myPassword);
// The view is changed to the index meaning the user has logged in
                RedirectView rv = new RedirectView();
            rv.setUrl("/index");
            return rv;}
//            The user doesnt exists and cannot log in
        }catch(Exception e){
            RedirectView rv = new RedirectView();
            rv.setUrl("/login-error");
            return rv;
        }

//        Error occured and go back to login
        RedirectView rv = new RedirectView();
        rv.setUrl("/login");
        return rv;



    }


    @RequestMapping(value = "/signUp", method  = {RequestMethod.POST})
    public RedirectView signUp(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("password2") String password2,HttpSession session) throws SQLException {
        try{
//            if passwords match, insert it as a new user using the user service.
        if (password.equals(password2)) {
            userService.insertUser(username, password);
            RedirectView rv = new RedirectView();
            rv.setUrl("/signUp-complete");
            return rv;
        }else{
            RedirectView rv = new RedirectView();
            rv.setUrl("/passwords-dont-match");
            return rv;
        }
        }catch(Exception e){
            RedirectView rv = new RedirectView();
            rv.setUrl("/generic-error");
            return rv;
        }
    }








    public void setUserService(com.Y2.Clinical.G12.services.userService userservice) {
        this.userService = userservice;
    }

    public com.Y2.Clinical.G12.services.userService getUserService() {
        return userService;
    }

}


