package com.Y2.Clinical.G12.controllers;

import com.Y2.Clinical.G12.data.questionnaires;
import com.Y2.Clinical.G12.data.questions;
import com.Y2.Clinical.G12.data.users;
import com.Y2.Clinical.G12.services.questionnaireService;
import com.Y2.Clinical.G12.services.usersAnswersService;
import org.owasp.html.Sanitizers;
import com.Y2.Clinical.G12.data.user_answer;

import com.Y2.Clinical.G12.services.userService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by c1632067 on 12/12/2017.
 */
@RestController
@RequestMapping (path = "/")
public class questionnaireController {


    @Autowired
    private com.Y2.Clinical.G12.services.userService userService;

    @Autowired
    private questionnaireService questionnaireService;

    @Autowired
    private com.Y2.Clinical.G12.services.questionService questionService;
    @Autowired
    private usersAnswersService UsersAnswersService;

    @Autowired
    public questionnaireController(questionnaireService questionnaireService) {
        this.setQuestionnaireService(questionnaireService);

    }

//    get a stream of all questionnaires
    @RequestMapping(path = "/allQuestionnaires", method = RequestMethod.GET)
    public List<questionnaires> returnQuestionnaires(HttpSession session) {
        List<questionnaires> questionnaires;
        questionnaires = getQuestionnaireService().returnQuestionnaires();
        return questionnaires.stream().collect(Collectors.toList());

    }

//    get the current user in the session to be displayed in the index header
    @RequestMapping(path = "/getUserSession", method = RequestMethod.GET)
    public List<users> getUserSession(HttpSession session) {
        String uname = (String) session.getAttribute("username");
        String pword = (String) session.getAttribute("password");
        Integer id = userService.getUser(uname,pword);
        List<users> thisUser = userService.getUserLoggedIn(id);
        return thisUser.stream().collect(Collectors.toList());
    }


//  insert a new questionnaire with the given title
    @RequestMapping(value = "/newQuestionnaire", method = RequestMethod.POST)
    public RedirectView insertQuestionnaire(@RequestParam("questionnaire") String questionnaire) {
        try{
        getQuestionnaireService().insertQuestionnaires(questionnaire);
        RedirectView rv = new RedirectView();
        rv.setUrl("/index");
        return rv;
        }catch(Exception e){
            RedirectView rv = new RedirectView();
            rv.setUrl("/generic-error");
            return rv;
        }

    }

//    delete the questionnaire with the title that matches
    @RequestMapping(value = "/Questionnaire/{questionnaireTitle}")
    public RedirectView deleteQuestionnaire(@PathVariable("questionnaireTitle") String questionnaireTitle) {
        try {
            getQuestionnaireService().deleteQuestionnaire(questionnaireTitle);
            RedirectView rv = new RedirectView();
            rv.setUrl("/index");
            return rv;
        }catch(Exception e){
            RedirectView rv = new RedirectView();
            rv.setUrl("/generic-error");
            return rv;
        }
    }



    @RequestMapping(value = "/completedQuestionnaire", method = RequestMethod.POST)
    public RedirectView completeQuestionnaire(@RequestParam("userAnswers") String userAnswers,@RequestParam("qId") String qId) {
        try {
//            put all answers into a list
            List<questions> allQuestions = getQuestionService().getQuestionsForQuestionnaire(qId);
            String[] allAnswersArray = userAnswers.split(",");
//            create the sanitiser policy
            org.owasp.html.PolicyFactory policy = Sanitizers.FORMATTING.and(Sanitizers.LINKS);
//           for each answer, check if it is safe html using the sanitiser
            for (int i = 0; i < (allQuestions.size()); i++) {
                String answer = allAnswersArray[i];
                String safe_answer = policy.sanitize(answer);
//                if not safe and 0 returned, change text to 'invalid input' and put that in the DB
                if (safe_answer.length() == 0) {
                    String final_answer = "Invalid Input";
                    UsersAnswersService.insertUsersAnswers(final_answer, i, 4);
//                if safe, insert into the db
                } else {
                    String final_answer = safe_answer;
                    UsersAnswersService.insertUsersAnswers(final_answer, i, 4);
                }
            }
//            change the view to the completed questionnaire page
            RedirectView rv = new RedirectView();
            rv.setUrl("/questionnaireComplete");
            return rv;
         }catch(Exception e) {
            RedirectView rv = new RedirectView();
            rv.setUrl("/generic-error");
            return rv;
        }
    }

    //  get a stream of all answers for the given questionnaire
    @RequestMapping(path = "/questionnaireAnswers", method = RequestMethod.GET)
    public List<user_answer> returnQuestionsWithTypes(@RequestParam("questionnaireId") String questionnaireId) {
        List<user_answer> answers;
        answers= UsersAnswersService.getUsersAnswers(questionnaireId);
        return answers.stream().collect(Collectors.toList());
    }










//ABSTRACTION METHODS

    public com.Y2.Clinical.G12.services.questionnaireService getQuestionnaireService() {
        return questionnaireService;
    }

    public void setQuestionnaireService(com.Y2.Clinical.G12.services.questionnaireService questionnaireService) {
        this.questionnaireService = questionnaireService;
    }

    public void setQuestionService(com.Y2.Clinical.G12.services.questionService questionService) {
        this.questionService = questionService;
    }

    public com.Y2.Clinical.G12.services.questionService getQuestionService() {
        return questionService;
    }


    public void setUserService(com.Y2.Clinical.G12.services.userService userservice) {
        this.userService = userservice;
    }

    public com.Y2.Clinical.G12.services.userService getUserService() {
        return userService;
    }



}
