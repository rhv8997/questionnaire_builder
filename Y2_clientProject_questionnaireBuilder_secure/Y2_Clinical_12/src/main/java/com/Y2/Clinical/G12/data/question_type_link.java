package com.Y2.Clinical.G12.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "question_type_link")
public class question_type_link {

    @Id
    @Column(name = "link_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer linkId;

    @Column(name = "question_type_question_type_id")
    private String QTQTID;

    @Column(name = "question_question_id")
    private String QTID;

}




