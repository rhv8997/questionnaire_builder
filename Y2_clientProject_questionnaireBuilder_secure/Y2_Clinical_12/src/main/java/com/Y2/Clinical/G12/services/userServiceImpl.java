package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.data.users;
import com.Y2.Clinical.G12.repositories.answerRepo;
import com.Y2.Clinical.G12.repositories.userRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service("UserService")
@Transactional
public class userServiceImpl implements userService {

    @Autowired
    private userRepository userRepo;

    @Autowired
    public userServiceImpl(answerRepo answerRepo){
        this.userRepo = userRepo;
    }


    @Override
    public Integer getUser(String myUsername, String myPassword) {
        return userRepo.getUser(myUsername,myPassword);

    }

    @Override
    public void insertUser(String username, String password) {
        userRepo.insertUser(username,password);
    }

    @Override
    public List<users> getUserLoggedIn(Integer id) {
        return userRepo.getUserLoggedIn(id);
    }


}



