// import com.Y2.Clinical.G12.data.questions;
// import com.Y2.Clinical.G12.mainApplication;
// import org.junit.Before;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.boot.test.json.JacksonTester;
// import org.springframework.boot.test.json.JsonContent;
// import org.springframework.boot.test.json.JsonContentAssert;
// import org.springframework.boot.test.web.client.TestRestTemplate;
// import org.springframework.core.ResolvableType;
// import org.springframework.http.HttpHeaders;
// import org.springframework.http.MediaType;
// import org.springframework.test.context.junit4.SpringRunner;
// import org.springframework.test.context.web.WebAppConfiguration;
// import org.springframework.test.web.servlet.MockMvc;
// import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
// import org.springframework.test.web.servlet.setup.MockMvcBuilders;
// import org.springframework.web.context.WebApplicationContext;
//
// import static org.assertj.core.api.Java6Assertions.assertThat;
// import static org.hamcrest.Matchers.containsString;
// import static org.hamcrest.core.IsEqual.equalTo;
// import static org.springframework.data.repository.init.ResourceReader.Type.JSON;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
// import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
// @SpringBootTest(classes= mainApplication.class)
// @RunWith(SpringRunner.class)
// @WebAppConfiguration
// public class APITests {
//
//     @Autowired
//     private WebApplicationContext ctx;
//
//
//     private MockMvc mockMvc;
//
//     private static final ResolvableType TYPE = ResolvableType
//             .forClass(questions.class);
//
//     @Before
//     public void setUp() {
//         this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
//     }
//
//     @Test
//     public void testQuestionAtIndex1() throws Exception {
//         mockMvc.perform(MockMvcRequestBuilders.get("/api/allQuestion/1").accept(MediaType.APPLICATION_JSON))
//                 .andExpect(status()
//                         .isOk())
//                 .andExpect(content()
//                         .string(equalTo("{\"questionId\":1,\"questionTitle\":\"How old are you?\",\"questionnaireId\":1}")));
//     }
//
//     @Test
//     public void testQuestionTitleAtIndex1() throws Exception {
//         mockMvc.perform(MockMvcRequestBuilders.get("/api/questionTitle/1").accept(MediaType.APPLICATION_JSON))
//                 .andExpect(status()
//                         .isOk())
//                 .andExpect(content()
//                         .string(equalTo("How old are you?")));
//     }
//
//     @Test
//     public void testGetQuestionnaireTitle() throws Exception {
//         mockMvc.perform(MockMvcRequestBuilders.get("/api/questionnaireTitle/1").accept(MediaType.APPLICATION_JSON))
//                 .andExpect(status()
//                         .isOk())
//                 .andExpect(content()
//                         .string(equalTo("Are you capable to adopt a child?")));
//     }
//
//     @Test
//     public void testGetQuestionCount() throws Exception {
//         mockMvc.perform(MockMvcRequestBuilders.get("/api/questionCount").accept(MediaType.APPLICATION_JSON))
//                 .andExpect(status()
//                         .isOk())
//                 .andExpect(content()
//                         .string(equalTo("3")));
//     }
//
//
//
//
//
// }
