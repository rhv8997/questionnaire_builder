// import com.Y2.Clinical.G12.data.questionnaires;
// import com.Y2.Clinical.G12.data.questions;
// import com.Y2.Clinical.G12.mainApplication;
// import com.Y2.Clinical.G12.repositories.questionnaireRepo;
// import org.aspectj.weaver.patterns.TypePatternQuestions;
// import org.junit.After;
// import org.junit.Before;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Qualifier;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
// import com.Y2.Clinical.G12.repositories.questionRepo;
// import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
// import java.util.List;
//
// import static org.junit.Assert.assertEquals;
//
//
// @RunWith(SpringJUnit4ClassRunner.class)
// @SpringBootTest(classes= mainApplication.class)
// public class HTTPTests {
//
//     @Qualifier("questionRepo")
//     @Autowired
//     private questionRepo qr;
//
//     @Qualifier("questionnaireRepo")
//     @Autowired
//     private questionnaireRepo qRepo;
//     @Before
//     public void setUp() {
//         createQuestionAndQuestionnaire();
//     }
//
//
//
//
//     @Test
//     public void getQuestionTitle() {
//         List<questions> allQuestions = qr.getQuestionsForQuestionnaire(1);
//         assertEquals("testing question", allQuestions.get(1).getQuestionTitle());
//     }
//
//
//
//     @Test
//     public void getQuestionnaireTitle() {
//         List<questionnaires> allQuestionnaires = qRepo.findAll();
//         assertEquals("Do you want to test medicine?", allQuestionnaires.get(1).getName());
//     }
//
//
//
//
//
//     public void createQuestionAndQuestionnaire(){
//         qr.insertQuestion("testing question",1,1);
//         qRepo.insertQuestionnaire("test questionnaire");
//         ;
//     }
//
//
//     @After
//     public void tearDown() {
//         String qt2 = "testing question";
//         String qt3 = "test questionnaire";
//
//         qr.testQuestionDeletion(qt2);
//
//         qRepo.testQuestionnaireDeletion(qt3);
//     }
//
//
// }
