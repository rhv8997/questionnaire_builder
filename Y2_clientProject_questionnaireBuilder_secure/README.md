To run the project, clone the repository from git@gitlab.cs.cf.ac.uk:c1639984/Y2_Clinical_12.git.

Once cloned, open up MySQL workbench and run the script called �db_Creation_Script� within
the local repository to create and populate tables and stored procedures.


If you are running the project on an Ubuntu virtual machine, open the project in IntelliJ 
and go to the application.properties, and uncomment the code for the database password and 
certificate that says for the Ubuntu VM. If using a Windows machine, uncomment the code for a Windows machine
 
To run the project from the command line, go into the root directory of the project and open command line.
 Type �Gradle clean� and then �Gradle build�. Once both have been completed, type �cd build� then �cd libs�
 and then �java �jar Y2_Clinical_12-0.0.1-SNAPSHOT.jar�. This will run the application.


You could also run the project in IntelliJ by going to the gradle toolbar on the right and under build folder, 
click clean and then build, and then bootRun under the application folder.
 
To view the application, open a web browser and type �https://localhost:8443/login�.
Here you can choose to create a user on the sign-up. 


