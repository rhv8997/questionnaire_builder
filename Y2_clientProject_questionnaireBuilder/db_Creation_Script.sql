-- MySQL Workbench Forward Engineering
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

/*HERE I DROP THE SCHEMA EVERY TIME I RUN THE SCRIPT, I THEN CREATE A NEW SCHEMA AND USE THAT NEW ONE*/
drop schema if exists clinical_survey;
CREATE SCHEMA IF NOT EXISTS `clinical_survey` DEFAULT CHARACTER SET utf8 ;
USE `clinical_survey` ;

-- -----------------------------------------------------
-- Table `clinical_survey`.`category`
-- -----------------------------------------------------
/*IN THIS TABLE, I HAVE THE CATEGORY ID AS THE PRIMARY KEY
THE ID WILL AUTO INCREMENT FROM 0
IT CONTAINS THE OTHER VALUE OF CATEGORY NAME*/
CREATE TABLE IF NOT EXISTS `clinical_survey`.`category` (
  `category_id` INT(11) NOT NULL AUTO_INCREMENT,
  `category_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE INDEX `categoryName_UNIQUE` (`category_name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clinical_survey`.`questionnaire_user`
-- -----------------------------------------------------
/*IN THIS TABLE, I HAVE THE USER ID AS THE PRIMARY KEY
THE ID WILL AUTO INCREMENT FROM 0
IT CONTAINS THE OTHER VALUES OF USERNAME AND PASSWORD
USERNAME IS A UNIQUE FIELD*/
CREATE TABLE IF NOT EXISTS `clinical_survey`.`questionnaire_user` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `password` text NOT NULL,
  `admin` boolean NOT NULL,
  `passExpire` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `username` (`username` ASC),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clinical_survey`.`questionnaire`
-- -----------------------------------------------------
/*IN THIS TABLE, I HAVE THE QUESTIONNAIRE ID AS THE PRIMARY KEY
THE ID WILL AUTO INCREMENT FROM 0
IT CONTAINS THE OTHER VALUES OF NAME, DATE CREATED, CATEGORY ID, AND USER ID
IT HAS MANY TO ONE RELATIONSHIPS BETWEEN CATEGORY AND USERS AND USES THEIR ID'S AS FOREIGN KEYS
THEY ARE REFERENCRES SO NEED TO BE CONSIDERED ON DELETION OF THE TABLE*/
CREATE TABLE IF NOT EXISTS `clinical_survey`.`questionnaire` (
  `questionnaire_id` INT(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_name` VARCHAR(45) NOT NULL,
  `date_created` DATE NOT NULL,
  `category_category_id` INT(11) NOT NULL,
  `questionnaire_user_user_id` INT(11) NOT NULL,
  PRIMARY KEY (`questionnaire_id`),
  UNIQUE INDEX `questionnaireId_UNIQUE` (`questionnaire_id` ASC),
  INDEX `fk_questionnaire_category1_idx` (`category_category_id` ASC),
  INDEX `fk_questionnaire_questionnaire_user1_idx` (`questionnaire_user_user_id` ASC),
  CONSTRAINT `fk_questionnaire_category1`
    FOREIGN KEY (`category_category_id`)
    REFERENCES `clinical_survey`.`category` (`category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_questionnaire_questionnaire_user1`
    FOREIGN KEY (`questionnaire_user_user_id`)
    REFERENCES `clinical_survey`.`questionnaire_user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clinical_survey`.`question`
-- -----------------------------------------------------
/*IN THIS TABLE, I HAVE THE QUESTION ID AS THE PRIMARY KEY
THE ID WILL AUTO INCREMENT FROM 0
IT CONTAINS THE OTHER VALUES OF QUESTION TITLE, QUESTIONNAIRE ID
IT HAS A MANY TO ONE RELATION TO THE TABLES QUESTIONNAIRE WHERE IT USES ITS ID AS FOREIGN KEY
THERE ARE REFERENCRE SO NEED TO BE CONSIDERED ON DELETION OF THE TABLE*/
CREATE TABLE IF NOT EXISTS `clinical_survey`.`question` (
  `question_id` INT(11) NOT NULL AUTO_INCREMENT,
  `question_title` VARCHAR(45) NOT NULL,
  `questionnaire_questionnaire_id` INT(11) NOT NULL,
  PRIMARY KEY (`question_id`),
  UNIQUE INDEX `questionId_UNIQUE` (`question_id` ASC),
  INDEX `fk_question_questionnaire1_idx` (`questionnaire_questionnaire_id` ASC),
  CONSTRAINT `fk_question_questionnaire1`
    FOREIGN KEY (`questionnaire_questionnaire_id`)
    REFERENCES `clinical_survey`.`questionnaire` (`questionnaire_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clinical_survey`.`muliple_choice_answer`
-- -----------------------------------------------------
/*THIS TABLE REPRESENTS THE ANSWERS SPECIFIED FOR MATRIX AND MULTI-CHOICE QUESTIONS
IN THIS TABLE, I HAVE THE ANSWER ID AS THE PRIMARY KEY
THE ID WILL AUTO INCREMENT FROM 0
IT CONTAINS THE OTHER VALUES OF ANSWER CONTENT AND QUESTION ID
IT HAS A MANY TO ONE LINK TO THE QUESTION TABLE WHICH USES ITS ID AS A FOREIGN KEY
THEY ARE REFERENCRES SO NEED TO BE CONSIDERED ON DELETION OF THE TABLE*/
CREATE TABLE IF NOT EXISTS `clinical_survey`.`muliple_choice_answer` (
  `answer_id` INT(11) NOT NULL AUTO_INCREMENT,
  `answer_content` VARCHAR(100) NOT NULL,
  `question_question_id` INT(11) NOT NULL,
  PRIMARY KEY (`answer_id`, `question_question_id`),
  UNIQUE INDEX `idAnswers_UNIQUE` (`answer_id` ASC),
  INDEX `fk_muliple_choice_answer_question1_idx` (`question_question_id` ASC),
  CONSTRAINT `fk_muliple_choice_answer_question1`
    FOREIGN KEY (`question_question_id`)
    REFERENCES `clinical_survey`.`question` (`question_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clinical_survey`.`question_type`
-- -----------------------------------------------------
/*IN THIS TABLE, I HAVE THE QUESTION TYPE AS THE PRIMARY KEY
THE ID WILL AUTO INCREMENT FROM 0
IT CONTAINS THE OTHER VALUES OF QUESTION ID AND TYPE NAME
TYPE NAME IS A UNIQUE FIELD*/
CREATE TABLE IF NOT EXISTS `clinical_survey`.`question_type` (
  `question_type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `type_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`question_type_id`),
  UNIQUE INDEX `typeName` (`type_name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clinical_survey`.`question_type_link`
-- -----------------------------------------------------
/*IN THIS TABLE, I HAVE THE LINK ID AS THE PRIMARY KEY
THE ID WILL AUTO INCREMENT FROM 0
IT CONTAINS THE OTHER VALUES OF QUESTION TYPE ID AND QUESTION ID
THIS IS A LINK TABLE WHICH HAS MANY TO ONE RELATIONSHIPS WITH QUESTION TYPE AND QUESTION AND USES THEIR IDS AS FOREIGN KEYS
THEY ARE REFERENCRES SO NEED TO BE CONSIDERED ON DELETION OF THE TABLE*/
CREATE TABLE IF NOT EXISTS `clinical_survey`.`question_type_link` (
  `link_id` INT(11) NOT NULL AUTO_INCREMENT,
  `question_type_question_type_id` INT(11) NOT NULL,
  `question_question_id` INT(11) NOT NULL,
  PRIMARY KEY (`link_id`, `question_type_question_type_id`, `question_question_id`),
  UNIQUE INDEX `idLink_UNIQUE` (`link_id` ASC),
  INDEX `fk_question_type_link_question_type1_idx` (`question_type_question_type_id` ASC),
  INDEX `fk_question_type_link_question1_idx` (`question_question_id` ASC),
  CONSTRAINT `fk_question_type_link_question1`
    FOREIGN KEY (`question_question_id`)
    REFERENCES `clinical_survey`.`question` (`question_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_type_link_question_type1`
    FOREIGN KEY (`question_type_question_type_id`)
    REFERENCES `clinical_survey`.`question_type` (`question_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `clinical_survey`.`user_answer`
-- -----------------------------------------------------
/*THIS TABLE RELATES TO THE ANSWERS FROM THE USERS
IN THIS TABLE, I HAVE THE ANSWER ID AS THE PRIMARY KEY
THE ID WILL AUTO INCREMENT FROM 0
IT CONTAINS THE OTHER VALUES OF ANSWER CONTENT AND QUESTION ID
IT HAS A MANY TO ONE LINK TO THE QUESTION TABLE WHICH USES ITS ID AS A FOREIGN KEY
THEY ARE REFERENCRES SO NEED TO BE CONSIDERED ON DELETION OF THE TABLE*/
CREATE TABLE IF NOT EXISTS `clinical_survey`.`user_answer` (
  `answer_id` INT(11) NOT NULL AUTO_INCREMENT,
  `answer_content` VARCHAR(100) NOT NULL,
  `question_question_id` INT(11) NOT NULL,
  PRIMARY KEY (`answer_id`, `question_question_id`),
  UNIQUE INDEX `idAnswers_UNIQUE` (`answer_id` ASC),
  INDEX `fk_user_answer_question1_idx` (`question_question_id` ASC),
  CONSTRAINT `fk_user_answer_question1`
    FOREIGN KEY (`question_question_id`)
    REFERENCES `clinical_survey`.`question` (`question_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
 use clinical_survey;
 
 /* Create a user account that only has limited privileges to limit what a user can do if they gain
 unauthorised access to the database*/
 
CREATE USER IF NOT EXISTS 'clinical'@'localhost' IDENTIFIED BY 'password';
GRANT select, insert, update, delete ON clinical_survey.* TO 'clinical'@'localhost';
FLUSH privileges;
 
 
/*******************************************************************************************************/
/*************************************INSERT TEST DATA**************************************************/
/*******************************************************************************************************/
 
insert into questionnaire_user values (1, 'admin','password', True, '2018-05-18 02:15:00');
insert into questionnaire_user values (2, 'username','password', False, '2018-05-18 02:15:00');

INSERT INTO question_type VALUES(1, "Short");
INSERT INTO question_type VALUES(2, "Long");
INSERT INTO question_type VALUES(3, "Matrix");
INSERT INTO question_type VALUES(4, "Multiple Choice");

INSERT INTO category VALUES (1, "Childcare");
INSERT INTO category VALUES (2, "Relationships");
INSERT INTO category VALUES (3, "Medical Trials");

insert  into questionnaire (questionnaire_id, questionnaire_name, date_created, category_category_id, questionnaire_user_user_id) VALUES (1, "Are you capable to adopt a child?", CURRENT_DATE, 1, 1);
insert into question values (1,"How old are you?",1);
Insert into user_answer values (1,"30",1);
insert into question_type_link values (1,1,1);

insert  into questionnaire (questionnaire_id, questionnaire_name, date_created, category_category_id, questionnaire_user_user_id) VALUES (2, "Do you want to test medicine?", CURRENT_DATE, 2, 1);
insert into question values (2,"Why do you want to test medicine?",2);
Insert into user_answer values (2,"I believe it is a vital aspect of medical development",2);
insert into question_type_link values (2,2,2);

insert  into questionnaire (questionnaire_id, questionnaire_name, date_created, category_category_id, questionnaire_user_user_id) VALUES (3, "Do you need marriage counciling?", '2017-01-01', 3, 1);
insert into question values (3,"Are you happy in your relationship?",3);
Insert into muliple_choice_answer values (1,"No",3);
Insert into muliple_choice_answer values (2,"Not Very",3);
Insert into muliple_choice_answer values (3,"Very",3);
Insert into muliple_choice_answer values (4,"Yes",3);

Insert into user_answer values (3,"-",3);
Insert into user_answer values (4,"-",3);
Insert into user_answer values (5,"x",3);
Insert into user_answer values (6,"-",3);

insert into question_type_link values (3,3,3);


create index indexQuestionTitle on question(question_title);



/*******************************************************************************************************/
/****************************************READ ME********************************************************/
/*******************************************************************************************************/

/*IN THE FIRST HALF OF THE SCRIPT, I HAVE WRITTEN SQL FOR THE CREATION OF STORED PROCEDURES, TRIGGERS, AND FUNCTIONS
SOME OF THESE STORED PROCEDURES, TRIGGER AND FUNCTIONS HAVE ALSO BEEN USED IN THE ADVANCES SQL QUERIES
*/


/*******************************************************************************************************/
/*************************************STORED PROCEDURES*************************************************/
/*******************************************************************************************************/

/*This is where i delete all of the existing stored procedures when i run the script*/
DROP PROCEDURE IF EXISTS insertQuestion; 
DROP PROCEDURE IF EXISTS insertQuestionnaire; 
DROP PROCEDURE IF EXISTS updateQuestionTitle; 
DROP procedure IF EXISTS allQuestionsForQuestionnaire; 


/*In this stored procedure, i insert a question record.
It takes the parameters and includes them in the insert query
I use this stored procedure when i want to insert a question
This stored procedure will also automatically activate the corresponding trigger (see below)*/
DELIMITER //
CREATE PROCEDURE insertQuestion(questionTitle VARCHAR(50), questionType INT, qId INT) 
	BEGIN 
		INSERT  INTO question (question_title,questionnaire_questionnaire_id) 
        VALUES (questionTitle,qId);        
 END// 
DELIMITER ;

/*In this stored procedure, i insert a questionnaire record
It takes the parameters and includes them in the insert query
As i want questionnaires to be unique, i first check if a questionnaire already exists with the same name i am trying to create one with
I use this stored procedure when i want to insert a questionnaire
This stored procedure will also automatically activate the corresponding trigger (see below)*/
DELIMITER //
CREATE PROCEDURE insertQuestionnaire(questionnaire VARCHAR(50))
	BEGIN 
	SET @Result = (SELECT COUNT(*) FROM questionnaire WHERE questionnaire_name= questionnaire);
	IF  @Result = 0 THEN 
		INSERT INTO questionnaire (questionnaire_id, questionnaire_name, date_created, category_category_id, questionnaire_user_user_id) VALUES (NULL, questionnaire, CURRENT_DATE, 1, 1);
	END IF;  
 END// 
DELIMITER ;

SET SQL_SAFE_UPDATES = 0;

/*In this stored procedure, i want to update a question
It takes in the old question i want to update and the new question title
These values are then put into the update query
I use this stored procedure when i want to update an existing question*/
DELIMITER //
CREATE PROCEDURE updateQuestionTitle(question VARCHAR(50), oldQuestion VARCHAR(50))
	BEGIN 
    SET FOREIGN_KEY_CHECKS=0; 
    UPDATE question 
    SET question_title =question 
    WHERE question_title = oldQuestion;
    END//
DELIMITER ;

/*In this stored procedure, i want to retrieve all of the questions for a certain questionnaire
It takes in to questionnaire id and uses it in the where clause
I use this stored procedure when i want to display all of the questions for a specific questionnaire*/

DROP procedure IF EXISTS getQuestionsForQuestionnaire; 

DELIMITER //
CREATE PROCEDURE getQuestionsForQuestionnaire(qId int)
	BEGIN 
    SET FOREIGN_KEY_CHECKS=0; 
    Select * from question where questionnaire_questionnaire_id = qId;
    end//
DELIMITER ;


DELIMITER ;


/*******************************************************************************************************/
/****************************************TRIGGERS*******************************************************/
/*******************************************************************************************************/

/*Here are all of the drop statements to drop all of the existing triggers*/
DROP TRIGGER IF EXISTS `delete question`;
DROP TRIGGER IF EXISTS `delete questionnaire`;
DROP TRIGGER IF EXISTS `insert question`;

/*Here i have written a trigger that will work whenever there is a question deletion
After a question is deleted, tables that have data about the question will also have the respective records deleted
The answers and the link table will have records deleted
This trigger is used when I want to delete a question from a database and avoids a constraint error*/
DELIMITER //
CREATE TRIGGER `delete question`     
  AFTER DELETE ON question    
  FOR EACH ROW     
BEGIN
  DELETE FROM question_type_link 
	WHERE question_question_id = OLD.question_id;
  DELETE FROM muliple_choice_answer 
	WHERE question_question_id = OLD.question_id;
    DELETE FROM user_answer 
	WHERE question_question_id = OLD.question_id;
END//
DELIMITER;

/*Here i have written a trigger that will work whenever there is a questionnaire deletion
After a questionnaire is deleted, tables that have data about the questionnaire will also have the respective records deleted
Questions will be deleted which will also acvivate the trigger above which will delete the respective records
This trigger is used when I want to delete a questionnaire from a database and avoids a constraint error*/
DELIMITER //
CREATE TRIGGER `delete questionnaire`     
  AFTER DELETE ON questionnaire    
  FOR EACH ROW     
BEGIN
  DELETE FROM question 
  WHERE questionnaire_questionnaire_id = OLD.questionnaire_id;

END//
DELIMITER;

/*Here i have written a trigger that will activate when i insert a short question
When i insert a short question, the question id will be inserted into the question_types_link table as well as the question type id
As i cannot pass a variable into the trigger, i cant change what the question_link_id will be so for now it is a default of 1 for short question
I use this trigger when i want to insert a question so it can automatically update the link table for me */

DELIMITER //
CREATE TRIGGER `insert question`     
  AFTER INSERT ON question    
  FOR EACH ROW     
BEGIN

	SET @id = 
    (SELECT question_id 
    FROM question 
    WHERE question_title= NEW.question_title);
    
    INSERT INTO question_type_link 
    VALUES(null, @id, 1);
END//
DELIMITER;

/*******************************************************************************************************/
/****************************************FUNCTIONS******************************************************/
/*******************************************************************************************************/
/*This is where i delete all of the existing functions when i run the script*/
DROP FUNCTION IF EXISTS number_of_questions_per_type;
DROP FUNCTION IF EXISTS question_type_name;
DROP FUNCTION IF EXISTS filter_to_get_first_questionaire_with_keyword;
/*In this function, i want to get the number of questions of a certain type
It takes in a value that is used in the where clause of the query to dertermine the question type
I set a variabke @count that would hold the value of the result
I used a join to have all of the right columns present so i could get the value
i the returned the count value to the place where it was called
I use this function when i want to know how many questions there are for a certain type of question*/
DELIMITER //
CREATE FUNCTION number_of_questions_per_type(ref INT) RETURNS int
BEGIN
SET @count=
   (SELECT COUNT(*)
	FROM question 
	INNER JOIN question_type_link 
		ON question.question_id = question_type_link.question_type_question_type_id 
	WHERE question_type_question_type_id=ref);
  RETURN @count;
END//

DELIMITER;

/*In this function, i want to get the name of certain types of question
It takes in a value that is used in the where clause of the query to dertermine the question type name
I set a variable @name that would hold the value of the result
I used a join to have all of the right columns present so i could get the value
I returned the value of the @name to where this function was called
I use this function when i want to know the name of a type of question*/

DELIMITER //
CREATE FUNCTION question_type_name(ref INT) RETURNS VARCHAR(45)
BEGIN
SET @name=
   (SELECT type_name
	FROM question_type 
	INNER JOIN question_type_link 
		ON question_type.question_type_id = question_type_link.question_type_question_type_id 
	WHERE question_type_id=ref);
  RETURN @name;
END//
DELIMITER;


/*In this function, i want to be able to search for a certian questionnaire
it takes in the input that is to be used and the output will be a text value for the questionnaire name
in the query, i have use a regex function that compare the input with the names of all the questionnaires in the table
it then return me the first value it gets back
I would change this query so that it return all questionnaire that match but i can only return a single value for now
I would use this function on the index page so i could search for a certain questionnaire*/
DELIMITER //
CREATE FUNCTION filter_to_get_first_questionaire_with_keyword(ref VARCHAR(45)) RETURNS VARCHAR(45)
BEGIN
SET @name=
  (SELECT questionnaire_name 
  FROM questionnaire 
  WHERE questionnaire_name REGEXP ref 
  LIMIT 1);
  RETURN @name;
END//

DELIMITER ;



/*******************************************************************************************************/
/****************************************EXAMPLES******************************************************/
/*******************************************************************************************************/


CALL insertQuestion('sp question',1,1);

CALL insertQuestionnaire('sp questionnaire');

CALL updateQuestionTitle('question update','sp question');

CALL getQuestionsForQuestionnaire(1);

select filter_to_get_first_questionaire_with_keyword('child');



/*EXAMPLE OF OTHER FUNCTIONS USED IN SQL_QUERY SCRIPT*/
/*EXAMPLE OF TRIGGER USED WHEN ACTIVATED*/



/*******************************************************************************************************/
/****************************************READ ME********************************************************/
/*******************************************************************************************************/

/*IN THE SECOND HALF OF THIS SCRIPT SCRIPT, I HAVE WRITTEN SQL FOR THE CREATION OF SOME QUERIES
SOME OF THESE QUERIES MAKE CALLS TO THE PROCEDURAL SQL*/


/*******************************************************************************************************/
/******************************************QUERIES******************************************************/
/*******************************************************************************************************/

/*This is where i delete all of the existing views when i run the script*/

DROP VIEW IF EXISTS `long or short questions`;
DROP VIEW IF EXISTS `All answers for all questions`;
DROP VIEW IF EXISTS `All answers for matrix question`;



/*Here i am creating a view that is selecting all the long and short questions within the database. I created this view so that when
i was returning all the questions from the questionnaires for the user to answer, i could know which questions would needed to have
text boxes for the user to answer

I used a join so that i could get the questions by using the link table and include it in an expression to get the questions with a
question type ID of either 1 or 2*/

CREATE VIEW `long or short questions` AS
SELECT question_title, question_type_question_type_id 
	FROM question 
	INNER JOIN question_type_link 
		ON question.question_id = question_type_link.question_type_question_type_id 
	WHERE question_type_question_type_id<=2;


/*With this select statement, i can call my view and include a where clause where it will only return me a short questions
I used this when i needed to decided if a question would need a single or multiline text box - for these questions, i needed a
single line text box*/
SELECT * FROM `long or short questions` WHERE question_type_question_type_id = 1;



/*In this select statement, i have used three unions. I have also called two functions which return me the name of a question type and how many questions there are of that type */        
SELECT question_type_name(1),number_of_questions_per_type(1)
 UNION 
SELECT question_type_name(2),number_of_questions_per_type(2)
 UNION 
SELECT question_type_name(3),number_of_questions_per_type(3)
 UNION 
SELECT question_type_name(4),number_of_questions_per_type(4);




/*In this query, i am using a case statement to compare when the questionnaires were created.
The case stement uses when to see if a expression is met and if so, then the outcome will be displayed
If there is a case where there is no date available, the else statement tells the user there is not date.
This could be used when a filter feature is implemented to retrieve questionnaires that were created in a certain year*/
SELECT questionnaire_id, date_created,
CASE
    WHEN date_created >=current_date() THEN "Questionnaire created THIS year"
    WHEN date_created < '2018-01-01'THEN "Questionnaire created LAST year"
    ELSE "Questionnaire date not found"
END
FROM questionnaire;

/*I created a view that will show me all of the users answer for the questions
I used an inner join that joined on the question_id
This is used when i want to be able to compare answers for certian questions.*/


CREATE VIEW `All answers for all questions` AS
	SELECT question_title, answer_content
	FROM question 
	INNER JOIN user_answer 
		ON question.question_id = user_answer.question_question_id;

/*I created a view that would display all of the default answers and user answers for the matrix questions
I used the As statement to rename the columns so i knew which column was for what type of answer
I used an inner join to get data from all of the tables involved
I used group by to collect all the records together
This is used when i want to return all of the questions answers from the questionnaires and compare answers*/

CREATE VIEW `All answers for matrix question` AS
	SELECT question_title, muliple_choice_answer.answer_content AS 'Matrix Answer',user_answer.answer_content AS 'User Answer' 
    FROM question 
	INNER JOIN user_answer 
		on question.question_id = user_answer.question_question_id
	INNER JOIN muliple_choice_answer 
		on question.question_id = muliple_choice_answer.question_question_id
	INNER JOIN question_type_link 
		on question.question_id = question_type_link.question_question_id
	WHERE question_type_link.question_type_question_type_id=3
		And muliple_choice_answer.question_question_id = 3
	GROUP BY muliple_choice_answer.answer_content
;

select * from `All answers for matrix question`;






