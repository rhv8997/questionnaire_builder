 import com.Y2.Clinical.G12.data.questions;
 import com.Y2.Clinical.G12.mainApplication;
 import org.json.JSONException;
 import org.junit.Before;
 import org.junit.Test;
 import org.junit.runner.RunWith;
 import org.skyscreamer.jsonassert.JSONAssert;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.boot.context.embedded.LocalServerPort;
 import org.springframework.boot.test.context.SpringBootTest;
 import org.springframework.boot.test.web.client.TestRestTemplate;
 import org.springframework.http.HttpEntity;
 import org.springframework.http.HttpHeaders;
 import org.springframework.http.HttpMethod;
 import org.springframework.http.ResponseEntity;
 import org.springframework.test.context.junit4.SpringRunner;
 import org.springframework.test.web.servlet.MockMvc;
 import org.springframework.test.web.servlet.setup.MockMvcBuilders;
 import org.springframework.web.context.WebApplicationContext;

 import javax.persistence.criteria.CriteriaBuilder;
 import java.util.List;

 import static org.junit.Assert.assertEquals;

 //ref http://www.springboottutorial.com/integration-testing-for-spring-boot-rest-services
 public class UnitTests {

     @Test
     public void createNewQuestionAndGetQuestionTitle() throws Exception {
         questions testQuestion = new questions(1,"I am a test",1);
         assertEquals("I am a test", testQuestion.getQuestionTitle());
     }


     @Test
     public void createNewQuestionAndGetQuestionId() throws Exception {
         questions testQuestion = new questions(1,"I am a test",1);
         assertEquals(1, Integer.parseInt(String.valueOf(testQuestion.getQuestionId())));
     }





 }



