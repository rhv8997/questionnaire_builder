package com.Y2.Clinical.G12.config;

import com.Y2.Clinical.G12.mainApplication;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

/**
 * Created by c1631256 on 22/03/2018.
 */
public class MyUnitTest {
    @Test
    public void testConcat(){
        MyUnit myUnit = new MyUnit();

        String result = myUnit.concat("one", "two");

        assertEquals("onetwo", result);

    }
}