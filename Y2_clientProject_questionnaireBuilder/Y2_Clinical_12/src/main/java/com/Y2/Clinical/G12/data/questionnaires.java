package com.Y2.Clinical.G12.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class represents the Questionnaires Entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "questionnaire")
public class questionnaires {

    @Id
    @Column(name = "questionnaire_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "questionnaire_name")
    private String name;

    @Column(name = "date_created")
    private String date;

    @Column(name = "category_category_id")
    private String categoryName;

    @Column(name = "questionnaire_user_user_id")
    private Long userId;
}
