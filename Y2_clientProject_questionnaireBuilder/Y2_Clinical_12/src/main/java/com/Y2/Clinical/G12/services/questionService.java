package com.Y2.Clinical.G12.services;


import com.Y2.Clinical.G12.data.questions;

import java.util.List;

public interface questionService {

    void insertQuestion(String questionTitle, Integer questionType, Integer qId);


    void deleteQuestion(String questionTitle);

    void updateQuestionTitle(String question, String oldQuestion);

    Integer getQuestionId(String question);

    List<questions> getQuestionsForQuestionnaire(Integer questionnaireId);

    questions getQuestionById(Integer ref);

    String getQuestionTitle(Integer ref);

    Integer getQuestionCount();



}


