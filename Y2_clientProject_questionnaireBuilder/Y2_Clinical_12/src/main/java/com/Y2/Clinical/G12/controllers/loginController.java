package com.Y2.Clinical.G12.controllers;

import com.Y2.Clinical.G12.services.userService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by c1632067 on 11/23/2017.
 */

@RestController
@RequestMapping(path = "/")
public class loginController {

    private static final Logger log = LoggerFactory.getLogger(loginController.class);

    @Autowired
    private userService userService;

    @ResponseBody
    @RequestMapping(value = "/myLogin", method  = {RequestMethod.GET, RequestMethod.POST})
    public RedirectView getUser(@RequestParam("username") String myUsername, @RequestParam("password") String myPassword) throws SQLException {
        Integer userId = getUserService().getUser(myUsername, myPassword);
        Boolean isAdmin = getUserService().getIsAdmin(myUsername);
        Date passExpireDate = getUserService().getPassExpire(myUsername);
        if (userId >= 1){
            DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();

            if (sdf.format(date).compareTo(sdf.format(passExpireDate)) > 0){
                log.error("User with ID {} logged in on {}", userId, sdf.format(date));
                log.warn("User with ID {} logged in on {}", userId, sdf.format(date));
                log.info("User with ID {} logged in on {}", userId, sdf.format(date));
                log.debug("User with ID {} logged in on {}", userId, sdf.format(date));

                if (isAdmin){
                    RedirectView rv = new RedirectView();
                    rv.setUrl("/admin");
                    return rv;
                }else {

                    RedirectView rv = new RedirectView();
                    rv.setUrl("/index");
                    return rv;
                }
            }else{
                RedirectView rv = new RedirectView();
                rv.setUrl("/updatePassService");
                return rv;
            }


        } else{
            RedirectView rv = new RedirectView();
            rv.setUrl("/login");
            return rv;
        }
    }


    public void setUserService(com.Y2.Clinical.G12.services.userService userservice) {
        this.userService = userservice;
    }

    public com.Y2.Clinical.G12.services.userService getUserService() {
        return userService;
    }

}


