package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.data.questions;
import com.Y2.Clinical.G12.repositories.questionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;


@Service("questionService")
@Transactional
public class questionServiceImpl implements questionService {

    @Autowired
    private questionRepo questionRepo;

    @Autowired
    public questionServiceImpl(questionRepo questionRepo){
        this.questionRepo = questionRepo;
    }

    @Override
    public void insertQuestion(String questionTitle, Integer questionType, Integer qId) {
        questionRepo.insertQuestion(questionTitle, questionType, qId);
    }


    @Override
    public void deleteQuestion(String questionTitle) {
        questionRepo.deleteQuestion(questionTitle);

    }

    @Override
    public void updateQuestionTitle(String question, String oldQuestion) {
        questionRepo.updateQuestionTitle(question, oldQuestion);
    }

    @Override
    public Integer getQuestionId(String question) {
        return questionRepo.getQuestionId(question);
    }

    @Override
    public List<questions> getQuestionsForQuestionnaire(Integer questionnaireId) {
        return questionRepo.getQuestionsForQuestionnaire(questionnaireId);
    }

    @Override
    public questions getQuestionById(Integer ref) {
        return questionRepo.getQuestionById(ref);
    }

    @Override
    public String getQuestionTitle(Integer ref) {
        return questionRepo.getQuestionTitle(ref);
    }

    @Override
    public Integer getQuestionCount() {
        return questionRepo.getQuestionCount();
    }


}



