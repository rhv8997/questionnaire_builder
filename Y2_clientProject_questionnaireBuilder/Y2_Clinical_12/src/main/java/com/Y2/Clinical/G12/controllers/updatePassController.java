package com.Y2.Clinical.G12.controllers;

import com.Y2.Clinical.G12.services.updatePassService;
import com.Y2.Clinical.G12.services.userService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.sql.SQLException;

/**
 * Created by c1631256 on 18/05/2018.
 */

@RestController
@RequestMapping(path = "/")
public class updatePassController {

    @Autowired
    private userService userService;

    @Autowired
    private updatePassService updatePassService;

    @ResponseBody
    @RequestMapping(value = "/updatePass", method = {RequestMethod.POST})
    public RedirectView updatePass(@RequestParam("username") String myUsername, @RequestParam("oldPassword") String oldPassword, @RequestParam("newPassword") String newPassword) throws SQLException{
        String actualOldPassword = getUserService().getOldPassword(myUsername);

        if (actualOldPassword == oldPassword){
            getUpdatePassService().updatePassword(myUsername,newPassword);
            RedirectView rv = new RedirectView();
            rv.setUrl("/login");
            return rv;
        }else{
            RedirectView rv = new RedirectView();
            rv.setUrl("/updatePassword");
            return rv;
        }

    }

    public com.Y2.Clinical.G12.services.userService getUserService() {
        return userService;
    }

    public com.Y2.Clinical.G12.services.updatePassService getUpdatePassService(){return updatePassService;}

}
