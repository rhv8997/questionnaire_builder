package com.Y2.Clinical.G12.repositories;

import com.Y2.Clinical.G12.data.users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

/**
 * Created by c1631256 on 18/05/2018.
 */
public interface updatePassRepo extends JpaRepository<users, Long> {

    @Autowired
    @Query(value = "update questionnaire_user set password=:newPassword where username=:myUsername", nativeQuery = true)
    @Transactional
    void updatePassword(@Param("myUsername") String myUsername, @Param("newPassword") String newPassword);
}
