package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.repositories.answerRepo;
import com.Y2.Clinical.G12.repositories.usersAnswersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service("UsersAnswerService")
@Transactional
public class usersAnswersServiceImpl implements usersAnswersService {

    @Autowired
    private usersAnswersRepo usersAnswersRepo;

    @Autowired
    public usersAnswersServiceImpl(answerRepo answerRepo){
        this.usersAnswersRepo = usersAnswersRepo;
    }

    @Override
    public void insertUsersAnswers(String answer, Integer questionId) {
        usersAnswersRepo.insertUsersAnswers(answer, questionId);

    }
}



