package com.Y2.Clinical.G12.repositories;


import com.Y2.Clinical.G12.data.users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

/**
 * Created by c1632067 on 11/30/2017.
 */

public interface userRepository extends JpaRepository<users, Long> {
    List<users> findAll();

    @Autowired
    @Query(value="Select count(*) from questionnaire_user where username=:myUsername AND password=:myPassword", nativeQuery = true)
    Integer getUser(@Param("myUsername")String myUsername,@Param("myPassword") String myPassword);

    @Autowired
    @Query(value="select admin from questionnaire_user where username=:myUsername", nativeQuery = true)
    Boolean getIsAdmin(@Param("myUsername")String myUsername);

    @Autowired
    @Query(value="select passExpire from questionnaire_user where username=:myUsername", nativeQuery = true)
    Date getPassExpire(@Param("myUsername")String myUsername);

    @Autowired
    @Query(value="select password from questionnaire_user where username=:myUsername", nativeQuery = true)
    String getOldPassword(@Param("myUsername")String myUsername);
}


