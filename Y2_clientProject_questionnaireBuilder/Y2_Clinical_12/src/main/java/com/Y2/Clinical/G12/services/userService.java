package com.Y2.Clinical.G12.services;


import java.sql.Date;

public interface userService {

    Integer getUser(String myUsername, String myPassword);

    Boolean getIsAdmin(String myUsername);

    Date getPassExpire(String myUsername);

    String getOldPassword(String myUsername);

}


