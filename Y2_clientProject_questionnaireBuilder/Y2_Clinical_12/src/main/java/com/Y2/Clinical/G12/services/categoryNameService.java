package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.data.categories;
import com.Y2.Clinical.G12.data.questionnaires;

import java.util.List;

/**
 * Created by c1632067 on 12/12/2017.
 */
public interface categoryNameService {

    List<questionnaires> getQuestionnaireByCategoryName(String categoryName);
    List<categories> getAllCategories();
}
