package com.Y2.Clinical.G12.repositories;

import com.Y2.Clinical.G12.data.answers;
import com.Y2.Clinical.G12.data.questions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by c1644043 on 30/11/2017.
 */
@Repository("answerRepo")
public interface answerRepo extends JpaRepository<answers, Long> {
    List<answers> findAll();


    @Modifying
    @Query(value = "{call insertAnswers(:answer, :questionId)}", nativeQuery = true)
    @Transactional
    void insertAnswers(@Param("answer") String answer, @Param("questionId")Integer questionId);

}


