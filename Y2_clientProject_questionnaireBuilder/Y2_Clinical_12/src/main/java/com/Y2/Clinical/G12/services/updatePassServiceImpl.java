package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.repositories.updatePassRepo;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by c1631256 on 18/05/2018.
 */
public class updatePassServiceImpl implements updatePassService {

    @Autowired
    private updatePassRepo updatePassRepo;

    @Override
    public void updatePassword(String myUsername, String newPassword){
        updatePassRepo.updatePassword(myUsername, newPassword);
    }
}
