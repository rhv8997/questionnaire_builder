package com.Y2.Clinical.G12.controllers;

import com.Y2.Clinical.G12.data.questionnaires;
import com.Y2.Clinical.G12.data.questions;
import com.Y2.Clinical.G12.services.questionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by c1632067 on 12/12/2017.
 */
@RestController
@RequestMapping (path = "/")
public class  questionnaireController {

    @Autowired
    private questionnaireService questionnaireService;

    @Autowired
    private com.Y2.Clinical.G12.services.questionService questionService;
    @Autowired
    private com.Y2.Clinical.G12.services.usersAnswersService usersAnswersService;

    @Autowired
    public questionnaireController(questionnaireService questionnaireService) {
        this.setQuestionnaireService(questionnaireService);

    }

    @RequestMapping(path = "/allQuestionnaires", method = RequestMethod.GET)
    public List<questionnaires> returnQuestionnaires() {
        List<questionnaires> questionnaires;
        questionnaires = getQuestionnaireService().returnQuestionnaires();
        return questionnaires.stream().collect(Collectors.toList());
    }

    @RequestMapping(path = "/allQuestionnairesByID", method = RequestMethod.GET)
    public List<questionnaires> returnQuestionnairesByID(Long ID) {
        List<questionnaires> questionnaires;
        questionnaires = getQuestionnaireService().returnQuestionnairesByID(ID);
        return questionnaires.stream().collect(Collectors.toList());
    }

    @RequestMapping(value = "/newQuestionnaire", method = RequestMethod.POST)
    public RedirectView insertQuestionnaire(@RequestParam("questionnaire") String questionnaire) {
        getQuestionnaireService().insertQuestionnaires(questionnaire);
        RedirectView rv = new RedirectView();
        rv.setUrl("/index");
        return rv;

    }

    @RequestMapping(value = "/Questionnaire/{questionnaireTitle}")
    public RedirectView deleteQuestionnaire(@PathVariable("questionnaireTitle") String questionnaireTitle) {
        getQuestionnaireService().deleteQuestionnaire(questionnaireTitle);
        RedirectView rv = new RedirectView();
        rv.setUrl("/index");
        return rv;
    }
//
    @RequestMapping(value = "/completedQuestionnaire", method = RequestMethod.POST)
    public RedirectView completeQuestionnaire(@RequestParam("userAnswers") String userAnswers,  @RequestParam("qId") Integer qId) {
        List<questions> allQuestions = getQuestionService().getQuestionsForQuestionnaire(qId);
        String[] allAnswersArray = userAnswers.split(",");
        for (int i = 0; i < (allQuestions.size()); i++) {
            String answer = allAnswersArray[i];
            getUsersAnswersService().insertUsersAnswers(answer,i);
    }
        RedirectView rv = new RedirectView();
        rv.setUrl("/questionnaireComplete");
        return rv;
    }









//ABSTRACTION METHODS

    public com.Y2.Clinical.G12.services.questionnaireService getQuestionnaireService() {
        return questionnaireService;
    }

    public void setQuestionnaireService(com.Y2.Clinical.G12.services.questionnaireService questionnaireService) {
        this.questionnaireService = questionnaireService;
    }

    public void setQuestionService(com.Y2.Clinical.G12.services.questionService questionService) {
        this.questionService = questionService;
    }

    public com.Y2.Clinical.G12.services.questionService getQuestionService() {
        return questionService;
    }

    public void setUsersAnswersService(com.Y2.Clinical.G12.services.usersAnswersService usersAnswersService) {
        this.usersAnswersService = usersAnswersService;
    }

    public com.Y2.Clinical.G12.services.usersAnswersService getUsersAnswersService() {
        return usersAnswersService;
    }




}
