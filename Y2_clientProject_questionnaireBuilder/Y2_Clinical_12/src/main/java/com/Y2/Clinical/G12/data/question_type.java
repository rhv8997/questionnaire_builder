package com.Y2.Clinical.G12.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "question_type")
public class question_type {

    @Id
    @Column(name = "question_type_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer questionTypeId;

    @Column(name = "type_name")
    private String typeName;


}




