package com.Y2.Clinical.G12.config;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class RequestRouter extends WebMvcConfigurerAdapter {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

        registry.addViewController("/questionnaireBuild/**").setViewName("forward:/questionnaireBuild.html");
        registry.addViewController("/index/**").setViewName("forward:/index.html");
        registry.addViewController("/login/**").setViewName("forward:/loginIndex.html");
        registry.addViewController("/answerQuestionnaire/**").setViewName("forward:/questionnaireRender.html");
        registry.addViewController("/questionnaireComplete/**").setViewName("forward:/questionnaireComplete.html");
        registry.addViewController("/admin/**").setViewName("forward:/adminSection.html");
        registry.addViewController("/updatePassword/**").setViewName("forward:/updatePassword.html");


    }
}