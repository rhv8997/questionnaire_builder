package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.repositories.answerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service("answerService")
@Transactional
public class answerServiceImpl implements answerService {

    @Autowired
    private answerRepo answerRepo;

    @Autowired
    public answerServiceImpl(answerRepo answerRepo){
        this.answerRepo = answerRepo;
    }


    @Override
    public void insertAnswers(String answer, Integer questionId) {
        answerRepo.insertAnswers(answer,questionId);
    }
}



