package com.Y2.Clinical.G12.repositories;

import com.Y2.Clinical.G12.data.questions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by c1644043 on 30/11/2017.
 */


@Repository("questionRepo")
public interface questionRepo extends JpaRepository<questions, Long> {

    @Modifying
    @Query(value = "{call insertQuestion(:questionTitle, :questionType,:qId)}", nativeQuery = true)
    @Transactional
    void insertQuestion(@Param("questionTitle") String questionTitle,@Param("questionType") Integer questionType,@Param("qId") Integer qId);



    List<questions> findAll();


    @Modifying
    @Query(value = "{call deleteQuestion(:questionTitle)}", nativeQuery = true)
    @Transactional
    void deleteQuestion(@Param("questionTitle") String questionTitle);


    @Modifying
    @Query(value = "{call updateQuestionTitle(:question,:oldQuestion)}", nativeQuery = true)
    @Transactional
    void updateQuestionTitle(@Param("question") String question, @Param("oldQuestion") String oldQuestion);

    @Modifying
    @Query(value = "{call testQuestionDeletion(:qt)}", nativeQuery = true)
    @Transactional
    void testQuestionDeletion(@Param("qt") String qt);


    @Autowired
    @Query(value="{call getQuestionId(:questionTitle)}", nativeQuery = true)
    Integer getQuestionId(@Param("questionTitle") String questionTitle);

    @Autowired
    @Query(value="{call getQuestionsForQuestionnaire(:questionnaireId)} ", nativeQuery = true)
    List<questions> getQuestionsForQuestionnaire(@Param("questionnaireId")Integer questionnaireId);



    @Autowired
    @Query(value="{call getQuestionById(:ref)} ", nativeQuery = true)
    questions getQuestionById(@Param("ref")Integer ref);

    @Autowired
    @Query(value="{call getQuestionTitle(:ref)} ", nativeQuery = true)
    String getQuestionTitle(@Param("ref")Integer ref);

    @Autowired
    @Query(value="{call getQuestionCount()} ", nativeQuery = true)
    Integer getQuestionCount();
}


