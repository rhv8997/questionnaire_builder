package com.Y2.Clinical.G12.model;

/**
 * Created by c1631256 on 30/11/2017.
 */
public class matrixQuestion {

    private String questions;
    private String answers;

    public String getQuestions() {
        return questions;
    }

    public void setQuestions(String questions) {
        this.questions = questions;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }
}
