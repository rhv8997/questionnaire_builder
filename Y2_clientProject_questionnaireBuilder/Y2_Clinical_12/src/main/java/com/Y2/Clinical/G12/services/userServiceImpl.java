package com.Y2.Clinical.G12.services;

import com.Y2.Clinical.G12.repositories.answerRepo;
import com.Y2.Clinical.G12.repositories.userRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;


@Service("UserService")
@Transactional
public class userServiceImpl implements userService {

    @Autowired
    private userRepository userRepo;

    @Autowired
    public userServiceImpl(answerRepo answerRepo){
        this.userRepo = userRepo;
    }


    @Override
    public Integer getUser(String myUsername, String myPassword) {
        return userRepo.getUser(myUsername,myPassword);

    }

    @Override
    public Boolean getIsAdmin(String myUsername){
        return userRepo.getIsAdmin(myUsername);
    }

    @Override
    public Date getPassExpire(String myUsername){
        return userRepo.getPassExpire(myUsername);
    }

    @Override
    public String getOldPassword(String myUsername){
        return userRepo.getOldPassword(myUsername);
    }
}



