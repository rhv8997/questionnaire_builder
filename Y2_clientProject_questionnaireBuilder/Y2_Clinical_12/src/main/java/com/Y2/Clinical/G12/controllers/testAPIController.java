package com.Y2.Clinical.G12.controllers;

import com.Y2.Clinical.G12.data.questions;
import com.Y2.Clinical.G12.repositories.questionRepo;
import com.Y2.Clinical.G12.services.answerService;
import com.Y2.Clinical.G12.services.questionService;
import com.Y2.Clinical.G12.services.questionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by c1632067 on 12/12/2017.
 */
@RestController
@RequestMapping (path = "/api")
public class testAPIController {

    @Autowired
    private questionService questionService;

    @Autowired
    private answerService answerService;

    @Autowired
    private questionnaireService questionnaireService;


    @Autowired
    public testAPIController(questionService questionService) {
        this.setQuestionService(questionService);
    }


    @ResponseBody
    @RequestMapping({"/allQuestion/{ref}"})
    public questions getQuestionRecord(@PathVariable Integer ref) {
        return getQuestionService().getQuestionById(ref);
    }


    @ResponseBody
    @RequestMapping({"/questionTitle/{ref}"})
    public String getQuestionTitle(@PathVariable Integer ref) {
        return getQuestionService().getQuestionTitle(ref);
    }

    @ResponseBody
    @RequestMapping({"/questionnaireTitle/{ref}"})
    public String getQuestionnaireTitle(@PathVariable Integer ref) {
        return getQuestionnaireService().getQuestionnaireTitle(ref);
    }


    @ResponseBody
    @RequestMapping({"/questionCount"})
    public Integer questionCount() {
        return getQuestionService().getQuestionCount();
    }
































    public com.Y2.Clinical.G12.services.questionnaireService getQuestionnaireService() {
        return questionnaireService;
    }

    public void setQuestionnaireService(com.Y2.Clinical.G12.services.questionnaireService questionnaireService) {
        this.questionnaireService = questionnaireService;
    }

    public void setQuestionService(com.Y2.Clinical.G12.services.questionService questionService) {
        this.questionService = questionService;
    }

    public com.Y2.Clinical.G12.services.questionService getQuestionService() {
        return questionService;
    }

    public void setAnswerService(com.Y2.Clinical.G12.services.answerService answerService) {
        this.answerService = answerService;
    }

    public com.Y2.Clinical.G12.services.answerService getAnswerService() {
        return answerService;
    }


}
